# [Markdown Reference Guide PDF][1]

This reference guide includes the basics of markdown. Topics covered:

1. General formatting (Bold and Italics)
2. Headers
3. Links (HTTP(S) and displaying media by extension)
4. Quotes
5. Horizontal line
6. Inline code
7. Block code
9. Ordered and unordered list



[1]:https://github.com/Az-21/learning-markdown/blob/master/reference/markdown-ref.pdf
